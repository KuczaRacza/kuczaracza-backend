use std::{
    convert::Infallible,
    str::{from_utf8, FromStr},
};

use serde::{Deserialize, Serialize};

use crate::{database, ArcCtx};
#[derive(Default, Debug, Serialize, Deserialize, Clone)]
struct Push {
    #[serde(skip_deserializing)]
    pub uuid: uuid::Uuid,
    pub user_name: String,
    pub user_email: String,
    pub project: Project,
    pub commits: Vec<Commit>,
}
#[derive(Default, Debug, Serialize, Deserialize, Clone)]
struct Project {
    pub id: u64,
    pub name: String,
    pub avatar_url: Option<String>,
    pub webb_url: String,
}
#[derive(Default, Debug, Serialize, Deserialize, Clone)]
struct Commit {
    pub hash: String,
    pub message: String,
    pub added: Vec<String>,
    pub modified: Vec<String>,
    pub removed: Vec<String>,
    pub timestamp: String,
}
#[derive(Default, Debug, Serialize, Deserialize, Clone)]
struct PushListElement {
    #[serde(skip_deserializing)]
    pub uuid: uuid::Uuid,
    pub project_id: u64,
    pub project_name: String,
    pub project_logo: Option<String>,
}

impl From<Push> for PushListElement {
    fn from(value: Push) -> Self {
        Self {
            uuid: value.uuid,
            project_id: value.project.id,
            project_name: value.project.name,
            project_logo: value.project.avatar_url,
        }
    }
}
impl TryFrom<database::Push> for PushListElement {
    type Error = serde_json::Error;
    fn try_from(value: database::Push) -> Result<Self, Self::Error> {
        serde_json::from_slice::<PushListElement>(&value.event).map(|mut push| {
            push.uuid = value.uuid;
            return push;
        })
    }
}

impl TryFrom<database::Push> for Push {
    type Error = serde_json::Error;
    fn try_from(value: database::Push) -> Result<Self, Self::Error> {
        serde_json::from_slice::<Push>(&value.event).map(|mut push| {
            push.uuid = value.uuid;
            return push;
        })
    }
}
pub async fn list_push_events(
    limit: u32,
    ctx: ArcCtx,
) -> Result<warp::reply::WithStatus<String>, Infallible> {
    if limit > 30 || limit == 0 {
        Ok(warp::reply::with_status(
            "Too big request".to_string(),
            warp::http::StatusCode::BAD_REQUEST,
        ))
    } else {
        let db = ctx.db.lock().await;
        Ok(warp::reply::with_status(
            serde_json::to_string(
                &db.list_push_events(limit)
                    .into_iter()
                    .map(|push_data| push_data.try_into().unwrap())
                    .collect::<Vec<PushListElement>>(),
            )
            .unwrap(),
            warp::http::StatusCode::ACCEPTED,
        ))
    }
}
pub async fn get_push_event(
    unformatted_uuid: String,
    ctx: ArcCtx,
) -> Result<warp::reply::WithStatus<String>, Infallible> {
    if let Ok(uuid) = uuid::Uuid::from_str(unformatted_uuid.as_str()) {
        let db = ctx.db.lock().await;

        if let Some(push_evenet_data) = db.get_push_event(&uuid) {
            let commit_event: Push = push_evenet_data.try_into().unwrap();
            Ok(warp::reply::with_status(
                serde_json::to_string(&commit_event).unwrap(),
                warp::http::StatusCode::ACCEPTED,
            ))
        } else {
            Ok(warp::reply::with_status(
                "not found".to_owned(),
                warp::http::StatusCode::BAD_REQUEST,
            ))
        }
    } else {
        Ok(warp::reply::with_status(
            "invalid uuid".to_owned(),
            warp::http::StatusCode::BAD_REQUEST,
        ))
    }
}
fn prepare_push_data(data: &[u8]) -> Result<database::Push, ()> {
    if let Ok(event) = from_utf8(data) {
        event.try_into().map_err(|_err| ())
    } else {
        Err(())
    }
}
pub async fn insert_push(
    ctx: ArcCtx,
    gitlab_token: String,
    event: warp::hyper::body::Bytes,
) -> Result<warp::reply::WithStatus<String>, Infallible> {
    let config = ctx.config.read().await;
    if gitlab_token == config.webhook_config.api_key {
        let db = ctx.db.lock().await;
        match prepare_push_data(&event) {
            Ok(push_data) => match db.insert_push_event(&push_data) {
                Ok(()) => Ok(warp::reply::with_status(
                    "accepted".to_owned(),
                    warp::http::StatusCode::ACCEPTED,
                )),
                Err(err) => {
                    eprintln!("Database error {}", err);
                    Ok(warp::reply::with_status(
                        "".to_owned(),
                        warp::http::StatusCode::INTERNAL_SERVER_ERROR,
                    ))
                }
            },
            Err(()) => Ok(warp::reply::with_status(
                "invalid request body".to_owned(),
                warp::http::StatusCode::FORBIDDEN,
            )),
        }
    } else {
        Ok(warp::reply::with_status(
            "invalid token".to_owned(),
            warp::http::StatusCode::FORBIDDEN,
        ))
    }
}
