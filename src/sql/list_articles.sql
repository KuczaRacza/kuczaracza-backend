SELECT "uuid",
    "link",
    "event"
FROM "articles"
ORDER BY "timestamp" DESC
LIMIT 1 ?