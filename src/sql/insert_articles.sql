INSERT
    OR REPLACE INTO "articles" ("uuid", "link", "event", "timestamp")
VALUES (?1, ?2, ?3, UNIXEPOCH());