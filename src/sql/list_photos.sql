SELECT "uuid",
    "event"
FROM "photos"
ORDER BY "timestamp" DESC
LIMIT ?1;