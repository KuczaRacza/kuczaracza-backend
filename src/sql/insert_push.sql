INSERT
    OR REPLACE INTO "pushes" (
        "uuid",
        "project_id",
        "event",
        "timestamp"
    )
VALUES (?1, ?2, ?3, UNIXEPOCH())