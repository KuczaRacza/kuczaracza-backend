SELECT "uuid",
    "project_id",
    "event"
FROM "pushes"
ORDER BY "_row_id_" DESC
LIMIT ?1;