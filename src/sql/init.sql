CREATE TABLE IF NOT EXISTS "pushes" (
    "uuid" BLOB,
    "project_id" INTEGER NOT NULL,
    "event" BLOB,
    "timestamp" UNSIGNED BIGINT,
    PRIMARY KEY ("uuid")
);
CREATE TABLE IF NOT EXISTS "photos" (
    "uuid" BLOB,
    "photo" BLOB,
    "event" BLOB,
    "timestamp" UNSIGNED BIGINT,
    PRIMARY KEY ("uuid")
);
CREATE TABLE IF NOT EXISTS "articles" (
    "uuid" BLOB,
    "link" TEXT,
    "event" BLOB,
    "timestamp" UNSIGNED BIGINT,
    PRIMARY KEY ("uuid")
);
CREATE TABLE IF NOT EXISTS "projects" (
    "uuid" BLOB,
    "link" TEXT,
    "event" BLOB,
    "timestamp" UNSIGNED BIGINT,
    PRIMARY KEY ("uuid")
);
CREATE VIEW IF NOT EXISTS "feed" AS
SELECT "uuid",
    "event",
    (
        SELECT "push"
    ) AS "category"
FROM "pushes"
UNION
SELECT "uuid",
    "event",
    (
        SELECT "photo"
    ) AS "category"
FROM "photos"
UNION
SELECT "uuid",
    "event",
    (
        SELECT "article"
    ) AS "category"
FROM "articles"
UNION
SELECT "uuid",
    "event",
    (
        SELECT "project"
    ) AS "category"
FROM "projects"