use warp::Filter;

use crate::{commits, context, photos, ArcCtx};
fn with_context(
    context: std::sync::Arc<context::Context>,
) -> impl Filter<Extract = (ArcCtx,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || context.clone())
}
fn list_push(
    ctx: ArcCtx,
) -> impl warp::Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("pushes" / "list" / u32)
        .and(warp::get())
        .and(with_context(ctx))
        .and_then(commits::list_push_events)
}
fn get_push(
    ctx: ArcCtx,
) -> impl warp::Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("pushes" / "get" / String)
        .and(warp::get())
        .and(with_context(ctx))
        .and_then(commits::get_push_event)
}
fn ping() -> impl warp::Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("ping")
        .and(warp::get())
        .map(|| "hello".to_string())
}

fn insert_push(
    ctx: ArcCtx,
) -> impl warp::Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("pushes" / "post")
        .and(warp::post())
        .and(with_context(ctx))
        .and(warp::header("X-Gitlab-Token"))
        .and(warp::body::content_length_limit(1024 * 15))
        .and(warp::body::bytes())
        .and_then(commits::insert_push)
}
fn insert_photo_event(
    ctx: ArcCtx,
) -> impl warp::Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("photos" / "insert" / "event")
        .and(warp::post())
        .and(with_context(ctx))
        .and(warp::body::content_length_limit(1024 * 15))
        .and(warp::body::bytes())
        .and_then(photos::insert_photo_event)
}
fn insert_photo_data(
    ctx: ArcCtx,
) -> impl warp::Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("photos" / "insert" / "data" / String)
        .and(warp::post())
        .and(with_context(ctx))
        .and(warp::body::content_length_limit(1024 * 1024 * 15))
        .and(warp::body::bytes())
        .and_then(photos::insert_photo_data)
}
pub fn routes(
    ctx: ArcCtx,
) -> impl warp::Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    list_push(ctx.clone())
        .or(ping())
        .or(get_push(ctx.clone()))
        .or(insert_photo_data(ctx.clone()))
        .or(insert_photo_event(ctx.clone()))
        .or(insert_push(ctx))
}
