use crate::{database, ArcCtx};
use image::io;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::prelude::*;
use std::{convert::Infallible, str::FromStr};

#[derive(Default, Debug, Serialize, Deserialize, Clone)]

pub struct Photo {
    #[serde(skip_deserializing)]
    pub uuid: uuid::Uuid,
    pub title: String,
    pub description: Option<String>,
    pub place: Option<String>,
    pub song_url: Option<String>,
}
impl From<Photo> for database::Photo {
    fn from(value: Photo) -> Self {
        Self {
            uuid: value.uuid,
            event: serde_json::to_vec(&value).unwrap(),
        }
    }
}

pub async fn insert_photo_event(
    ctx: ArcCtx,
    body: warp::hyper::body::Bytes,
) -> Result<warp::reply::WithStatus<String>, Infallible> {
    if let Ok(mut photo) = serde_json::from_slice::<Photo>(&body) {
        photo.uuid = uuid::Uuid::new_v4();
        let db = ctx.db.lock().await;
        let uuid = photo.uuid.to_string();
        if db.insert_photo_event(&photo.into()).is_err() {
            Ok(warp::reply::with_status(
                "database error".to_owned(),
                warp::http::StatusCode::INTERNAL_SERVER_ERROR,
            ))
        } else {
            Ok(warp::reply::with_status(
                uuid,
                warp::http::StatusCode::ACCEPTED,
            ))
        }
    } else {
        Ok(warp::reply::with_status(
            "invalid data".to_owned(),
            warp::http::StatusCode::INTERNAL_SERVER_ERROR,
        ))
    }
}
pub async fn insert_photo_data(
    unforamtted_uuid: String,
    ctx: ArcCtx,
    body: warp::hyper::body::Bytes,
) -> Result<warp::reply::WithStatus<String>, Infallible> {
    let image_uuid: uuid::Uuid;
    match uuid::Uuid::from_str(&unforamtted_uuid) {
        Ok(new_uuid) => image_uuid = new_uuid,
        Err(_err) => {
            return Ok(warp::reply::with_status(
                "invalid uuid".to_owned(),
                warp::http::StatusCode::INTERNAL_SERVER_ERROR,
            ))
        }
    }
    let mut file = std::fs::File::create("foo.txt").unwrap();
    file.write_all(&body).unwrap();
    match image::load_from_memory(&body) {
        Ok(image) => {
            if image.width() * image.height() > 50000000 {
                Ok(warp::reply::with_status(
                    "image is too large".to_owned(),
                    warp::http::StatusCode::INTERNAL_SERVER_ERROR,
                ))
            } else {
                let db = ctx.db.lock().await;
                match webp::Encoder::from_image(&image) {
                    Ok(webp_image) => match webp_image.encode_simple(false, 80.0) {
                        Ok(encoded) => {
                            let webp_bytes = encoded.to_vec();
                            match db.insert_photo_data(&image_uuid, &webp_bytes) {
                                Ok(()) => Ok(warp::reply::with_status(
                                    "accepted".to_owned(),
                                    warp::http::StatusCode::ACCEPTED,
                                )),
                                Err(err) => {
                                    eprintln!("{:?}", err);
                                    Ok(warp::reply::with_status(
                                        "database error".to_owned(),
                                        warp::http::StatusCode::INTERNAL_SERVER_ERROR,
                                    ))
                                }
                            }
                        }
                        Err(_err) => Ok(warp::reply::with_status(
                            "cannot convert".to_owned(),
                            warp::http::StatusCode::INTERNAL_SERVER_ERROR,
                        )),
                    },
                    Err(_err) => Ok(warp::reply::with_status(
                        "cannot convert".to_owned(),
                        warp::http::StatusCode::INTERNAL_SERVER_ERROR,
                    )),
                }
            }
        }
        Err(err) => {
            eprintln!("image error {:?}", err);
            Ok(warp::reply::with_status(
                "image is corrupted".to_owned(),
                warp::http::StatusCode::INTERNAL_SERVER_ERROR,
            ))
        }
    }
}

pub async fn get_photo_event(
    ctx: ArcCtx,
    unformated_uuid: &str,
) -> Result<warp::reply::WithStatus<String>, Infallible> {
    
}
