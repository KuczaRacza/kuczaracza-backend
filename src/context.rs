use crate::{config::Config, database};
use tokio::sync;

pub struct Context {
    pub db: sync::Mutex<database::Database>,
    pub config: sync::RwLock<Config>,
}
impl Context {
    pub fn new(config_path: &std::path::Path) -> Self {
        let config = Config::load(config_path);
        Self {
            db: sync::Mutex::new(database::Database::new(std::path::Path::new(
                &config.database.database_path,
            ))),
            config: sync::RwLock::new(config),
        }
    }
}
