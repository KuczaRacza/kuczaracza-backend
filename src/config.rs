use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize, Debug)]
pub struct WebhookConfig {
    pub endpoint: String,
    pub api_key: String,
}
#[derive(Serialize, Deserialize, Debug)]
pub struct Database {
    pub database_path: String,
}
#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub webhook_config: WebhookConfig,
    pub database: Database,
}

impl Config {
    pub fn load(path: &std::path::Path) -> Self {
        serde_json::from_str::<Config>(std::fs::read_to_string(path).unwrap().as_str()).unwrap()
    }
}
