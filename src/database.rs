use std::path::Path;

use rusqlite::params;
use serde::{Deserialize, Serialize};

pub struct Database {
    pub connection: rusqlite::Connection,
}
static SQL_INIT_TABLES: &'static str = include_str!("./sql/init.sql");
static SQL_LIST_COMMITS: &'static str = include_str!("./sql/list_pushes.sql");
static SQL_GET_COMMIT: &'static str = include_str!("./sql/get_push.sql");
static SQL_INSERT_COMMIT: &'static str = include_str!("./sql/insert_push.sql");
static SQL_INSERT_PHOTO_DATA: &'static str = include_str!("./sql/insert_photo_data.sql");
static SQL_INSERT_PHOTO_EVENT: &'static str = include_str!("./sql/insert_photo_event.sql");
static SQL_GET_PHOTO_EVENT: &'static str = include_str!("./sql/get_photo_event.sql");

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct Push {
    #[serde(skip_deserializing)]
    pub uuid: uuid::Uuid,
    pub project_id: u64,
    #[serde(skip_deserializing)]
    pub event: Vec<u8>,
}
#[derive(Default, Debug, Clone)]
pub struct Photo {
    pub uuid: uuid::Uuid,
    pub event: Vec<u8>,
}

impl TryFrom<&str> for Push {
    type Error = serde_json::Error;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut res: Push = serde_json::from_str(value)?;
        res.uuid = uuid::Uuid::new_v4();
        res.event = Vec::<u8>::from(value);
        Ok(res)
    }
}

impl Database {
    pub fn new(path: &Path) -> Self {
        let db = Self {
            connection: rusqlite::Connection::open(path).unwrap(),
        };

        db.connection.execute_batch(SQL_INIT_TABLES).unwrap();
        db
    }
    pub fn list_push_events(&self, limit: u32) -> Vec<Push> {
        self.connection
            .prepare(SQL_LIST_COMMITS)
            .unwrap()
            .query([limit])
            .unwrap()
            .mapped(|row| {
                Ok(Push {
                    uuid: uuid::Uuid::from_bytes(row.get(0)?),
                    project_id: row.get(1)?,
                    event: row.get(2)?,
                })
            })
            .map(|row| row.unwrap())
            .collect()
    }
    pub fn get_push_event(&self, uuid: &uuid::Uuid) -> Option<Push> {
        self.connection
            .prepare(SQL_GET_COMMIT)
            .unwrap()
            .query([uuid.as_bytes()])
            .unwrap()
            .mapped(|row| {
                Ok(Push {
                    uuid: uuid::Uuid::from_bytes(row.get(0)?),
                    project_id: row.get(1)?,
                    event: row.get(2)?,
                })
            })
            .map(|row| row.unwrap())
            .next()
    }
    pub fn insert_push_event(&self, commit: &Push) -> Result<(), rusqlite::Error> {
        self.connection
            .execute(
                SQL_INSERT_COMMIT,
                params![&commit.uuid.as_bytes(), &commit.project_id, &commit.event,],
            )
            .map(|_ok| ())
    }
    pub fn insert_photo_event(&self, photo: &Photo) -> Result<(), rusqlite::Error> {
        self.connection
            .execute(
                SQL_INSERT_PHOTO_EVENT,
                params![&photo.uuid.as_bytes(), &photo.event],
            )
            .map(|_ok| ())
    }
    //FIXME returns ok even if image was not added
    pub fn insert_photo_data(
        &self,
        uuid: &uuid::Uuid,
        photo: &[u8],
    ) -> Result<(), rusqlite::Error> {
        self.connection
            .execute(SQL_INSERT_PHOTO_DATA, params![&photo, &uuid.as_bytes()])
            .map(|_ok| ())
    }
    pub fn get_photo_event(&self, uuid: &uuid::Uuid) -> Result<Photo, rusqlite::Error> {
        self.connection
            .prepare(SQL_GET_PHOTO_EVENT)?
            .query(params![uuid.as_bytes()])?
            .mapped(|row| {
                Ok(Photo {
                    uuid: uuid::Uuid::from_bytes(row.get(0)?),
                    event: row.get(1)?,
                })
            })
            .next()
    }
}
