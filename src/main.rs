use warp::Filter;

mod commits;
mod config;
mod context;
mod database;
mod routes;
mod tests;
mod photos;

type ArcCtx = std::sync::Arc<context::Context>;
#[tokio::main]
async fn main() {
    let context = std::sync::Arc::new(context::Context::new(std::path::Path::new("./config.json")));
    

    warp::serve(routes::routes(context))
        .run(([127, 0, 0, 1], 3030))
        .await;
}
